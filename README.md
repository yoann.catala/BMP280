# Modifications apportées

## Adafruit_BMN280.h

Original

>       class Adafruit_BME280 {
>           public:
>           ...
>               Adafruit_BMP280();
>           ...
>       }

Modifié

>       class Adafruit_BME280 {
>           public:
>           ...
>               Adafruit_BMP280(uint8_t sda, uint8_t scl);
>           ...
>       }

Ajout des variables pinSDA et pinSCL

>       class Adafruit_BME280 {
>           private:
>           ...
>               uint8_t pinSDA;
>               uint8_t pinSCL;
>           ...
>       }

## Adafruit_BMN280.cpp

Original

>       Adafruit_BMP280::Adafruit_BMP280(){
>           : _cs(-1), _mosi(-1), _miso(-1), _sck(-1)
>       }

Modifié

>       Adafruit_BMP280::Adafruit_BMP280(uint8_t sda, uint8_t scl){
>           : _cs(-1), _mosi(-1), _miso(-1), _sck(-1), pinSDA(sda), pinSCL(scl)
>       }